﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class PublicViewModel 
    {
        public Competition SelectedCompetition { get; set; }
        public CompetitionSubmission CompSubmission { get; set; }
        public List<CompetitionSubmission> CompetitionSubmissionList { get; set; }
        public Comment comment { get; set; }
        public List<Comment> commentList { get; set; }
        public List<Competitor> competitorList { get; set; }
    }
}
