﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class AreaInterest
    {
        public int AreaInterestID { get; set; }

        [Display(Name="Name of Area of Interest")]
        [StringLength(50,ErrorMessage ="Name of area of interest cannot be more than 50 characters.")]
        [Required(ErrorMessage ="Please enter a name.")]
        [ValidateAINameExists]
        public string Name { get; set; }
    }
}
