﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class CompetitionScoreViewModel
    {
        public Competition SelectedCompetition { get; set; }
        public CompetitionSubmission CompSubmission { get; set; }
        public List<CriteriaScore> CriteriaScoreList { get; set; }
    }
    public class CriteriaScore
    {
        public int CriteriaID { get; set; }
        public int CompetitorID { get; set; }
        public int CompetitionID { get; set; }
        public string CriteriaName { get; set; }
        public int Score { get; set; }

    }
}