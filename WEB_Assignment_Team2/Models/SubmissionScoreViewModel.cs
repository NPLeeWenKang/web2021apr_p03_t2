﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class SubmissionScoreViewModel
    {
        public List<CompetitionScore> CsList { get; set; }
        public CompetitionSubmission CompSub { get; set; }
        public List<Criteria> CriteriaList { get; set; }
    }
}
