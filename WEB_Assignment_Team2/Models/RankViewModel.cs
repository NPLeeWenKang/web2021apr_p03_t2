﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class RankViewModel
    {
        public SubmissionScoreViewModel SubmissionScoreVM { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.#}")]
        public double TotalScore { get; set; }
        public SubmissionViewModel SubmissionVM { get; set; }

    }
}
