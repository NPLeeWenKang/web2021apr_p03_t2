﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class Competition
    {
        [Display(Name ="Competition ID")]
        public int CompetitionID { get; set; }
        [Display(Name ="Area of Interest ID")]
        public int AreaInterestID { get; set; }
        [Display(Name ="Competition Name")]
        [StringLength(255, ErrorMessage = "Name of competition cannot be more than 255 characters.")]
        [Required(ErrorMessage = "Please enter a name.")]
        public string CompetitionName { get; set; }
        [Display(Name ="Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: d-MMM-yyyy}")]
        public DateTime? StartDate { get; set; }
        [Display(Name ="End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0: d-MMM-yyyy}")]
        public DateTime? EndDate { get; set; }
        [Display(Name ="Result Released Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: d-MMM-yyyy}")]
        public DateTime? ResultReleasedDate { get; set; }
    }
}
