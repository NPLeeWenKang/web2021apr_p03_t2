﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class SubmissionViewModel
    {
        public Competition SelectedCompetition { get; set; }
        public List<CompetitionSubmission> CompetitionSubmissionList { get; set; }
        public List<Competitor> CompetitorList { get; set; }
    }
}
