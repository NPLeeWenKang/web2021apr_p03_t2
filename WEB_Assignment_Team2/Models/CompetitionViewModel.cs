﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class CompetitionViewModel
    {
        public List<Competition> ListCompetition { get; set; }
        public List<AreaInterest> AiList { get; set; }
        public CompetitionJudgeViewModel CompJudgeVM { get; set; }
    }
}
