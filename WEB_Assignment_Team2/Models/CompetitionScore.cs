﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class CompetitionScore
    {
        public int CriteriaID { get; set; }
        public int CompetitorID { get; set; }
        public int CompetitionID { get; set; }
        [Required(ErrorMessage ="Please enter a score")]
        [Range(0,10,ErrorMessage = "Score is out of range.")]
        public int Score { get; set; }
        [Display(Name ="Date and time of last edit.")]
        public DateTime? DateTimeLastEdit { get; set; }
    }
}
