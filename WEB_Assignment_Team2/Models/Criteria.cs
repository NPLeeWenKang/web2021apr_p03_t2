﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class Criteria
    {
        public int CriteriaID { get; set; }
        public int CompetitionID { get; set; }
        [Display(Name="Criteria Name")]
        [StringLength(50,ErrorMessage ="Name is more than 50 characters.")]
        [Required(ErrorMessage ="Please enter criteria's name.")]
        public string CriteriaName { get; set; }
        [Display(Name ="Weightage (%)")]
        [Required(ErrorMessage = "Please enter criteria's weightage.")]
        [Range(1,100,ErrorMessage ="Weightage not within range.")]
        public int Weightage { get; set; }
    }
}
