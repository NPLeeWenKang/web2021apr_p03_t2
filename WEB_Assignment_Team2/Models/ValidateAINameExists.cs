﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB_Assignment_Team2.DAL;

namespace WEB_Assignment_Team2.Models
{
    public class ValidateAINameExists : ValidationAttribute
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            string name = Convert.ToString(value);
            AreaInterest areaInterest = (AreaInterest)validationContext.ObjectInstance;
            int areaInterestId = areaInterest.AreaInterestID;

            if (aiContext.IsNameExist(name, areaInterestId))
                // validation failed
                return new ValidationResult("Name already exists!");
            else
                // validation passed 
                return ValidationResult.Success;
        }
    }
}
