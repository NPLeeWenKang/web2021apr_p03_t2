﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class CompetitionJudgeViewModel
    {
        public Competition SelectedCompetition { get; set; }
        public List<Judge> JudgeList { get; set; }
    }
}
