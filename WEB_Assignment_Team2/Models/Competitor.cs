﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace WEB_Assignment_Team2.Models
{
    public class Competitor
    {
        [Display(Name = "Id")]
        public int CompetitorId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter your name.")]
        [StringLength(50, ErrorMessage ="Maximum of 50 charactors allowed.")]
        public string CompetitorName { get; set; }

        [Display(Name = "Salutation")]
        [StringLength(5, ErrorMessage = "Maximum of 5 charactors allowed.")]
        public string? Salutation { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Please enter your email.")]
        [StringLength(50, ErrorMessage = "Maximum of 50 charactors allowed.")]
        public string EmailAddr { get; set; }

        [Display(Name = "Password")]
        [StringLength(255, ErrorMessage = "Maximum of 255 charactors allowed.")]
        public string Password { get; set; } 
    }
}
