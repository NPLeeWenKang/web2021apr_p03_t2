﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int CompetitionID { get; set; }

        [Required(ErrorMessage = "Please enter a comment")]
        [StringLength(255, ErrorMessage = "Maximum of 255 characters allowed.")]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString ="{0:yyyy-MM-dd HH:mm}")]
        public DateTime DateTimePosted { get; set; }

    }
}
