﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class CriteriaViewModel
    {
        public Competition SelectedCompetition { get; set; }
        public List<Criteria> CriteriaList { get; set; }
        public string Command { get; set; }
    }
}
