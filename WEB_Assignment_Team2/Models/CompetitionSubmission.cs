﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WEB_Assignment_Team2.Models
{
    public class CompetitorSubmission
    {
        public Competition SelectedCompetition { get; set; }
        public CompetitionSubmission SelectedCompetitionSubmission { get; set; }
        public IFormFile fileToUpload { get; set; }
    }
    public class CompetitionSubmission
    {
        [Display(Name = "Competition ID")]
        public int CompetitionID { get; set; }

        [Display(Name = "Competitor ID")]
        public int CompetitorID { get; set; }

        [Display(Name = "File name")]
        [StringLength(255 , ErrorMessage = "Maximum number of charactors is 255")]
        public string FileSubmitted { get; set; }

        [Display(Name = "Date file uploaded")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: d-MMM-yyyy}")]
        public DateTime? DateTimeFileUpload { get; set; }

        [StringLength(255, ErrorMessage = "Maximum number of charactors is 255")]
        [Display(Name = "Appeal")]
        public string Appeal { get; set; }

        [Display(Name = "Number of votes")]
        public int VoteCount { get; set; }

        [Display(Name = "Ranking")]
        public int? Ranking { get; set; }
    }
}
