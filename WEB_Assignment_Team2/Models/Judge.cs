﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_Assignment_Team2.Models
{
    public class Judge
    {
        [Display(Name = "Judge ID")]
        public int JudgeID { get; set; }
        [Display(Name = "Judge Name")]
        [Required(ErrorMessage ="Please enter your name.")]
        [StringLength(50,ErrorMessage ="Name cannot exceed 50 characters.")]
        public string JudgeName { get; set; }
        public string Salutation { get; set; }
        [Display(Name ="Area of Interest")]
        [Required(ErrorMessage = "Please enter your area of interest.")]
        public int AreaInterestID { get; set; }
        [Display(Name = "Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@lcu.edu.sg")]
        [StringLength(50, ErrorMessage = "Email cannot exceed 50 characters.")]
        [Required(ErrorMessage = "Please enter your email.")]
        public string EmailAddr { get; set; }
        [Display(Name ="Password")]
        [StringLength(255,ErrorMessage ="Password cannot exceed 255 characters.")]
        public string Password { get; set; }
    }
}
