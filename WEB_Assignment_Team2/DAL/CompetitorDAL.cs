﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    
    public class CompetitorDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CompetitorDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public Competitor GetCompetitor(int competitorId)
        {
            // gets a competitor details
            Competitor targetCompetitor = null;
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Competitor Where CompetitorID = @competitorID";
            cmd.Parameters.AddWithValue("@competitorID", competitorId);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    targetCompetitor = new Competitor
                    {
                        CompetitorId = reader.GetInt32(0),
                        CompetitorName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        EmailAddr = reader.GetString(3).ToLower(),
                        Password = reader.GetString(4)
                    };
                }
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return targetCompetitor;
        }
        public int UpdateProfile(Competitor competitor)
        {
            // updates a competitor's profile detail
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Competitor SET CompetitorName=@name,
Salutation=@salutation 
WHERE CompetitorID = @selectedCompetitorID";
            cmd.Parameters.AddWithValue("@name", competitor.CompetitorName);
            if (competitor.Salutation == null)
            {
                cmd.Parameters.AddWithValue("@salutation", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@salutation", competitor.Salutation);
            }
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitor.CompetitorId);
            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();
            return count;
        }
        public int UpdatePassword(int id, string password)
        {
            // updatesa competitor's password
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Competitor SET Password=@password
WHERE CompetitorID = @selectedCompetitorID";
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", id);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }
        public bool EmailValidate(string email)
        {
            // validates a competitor's email
            bool emailFound;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor
            WHERE Lower(EmailAddr) = Lower(@selectedEmail)";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                emailFound = true;
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();
            return emailFound;
        }

        public int AddCompetitor(Competitor c)
        {
            // adds new competitors to database
            bool emailFound = EmailValidate(c.EmailAddr);
            if (emailFound == true)
            {
                return -1;
            }

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            cmd.CommandText = @"INSERT INTO Competitor (CompetitorName, Salutation, EmailAddr, Password)
            OUTPUT INSERTED.CompetitorID
            VALUES(@name, @Salutation, @email, @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", c.CompetitorName);
            if (c.Salutation == "")
            {
                cmd.Parameters.AddWithValue("@salutation", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@salutation", c.Salutation);
            }
            cmd.Parameters.AddWithValue("@email", c.EmailAddr);
            cmd.Parameters.AddWithValue("@password", c.Password);
            //A connection to database must be opened before any operations made.
            conn.Open();
            c.CompetitorId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return c.CompetitorId;
        }
        public Competitor AuthenticateCompetitor(string email, string password)
        {
            // authenticate competitors using password and email
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor
            WHERE Lower(EmailAddr) Like Lower(@selectedEmail)";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    Competitor c = new Competitor
                    {
                        CompetitorId = reader.GetInt32(0),
                        CompetitorName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        EmailAddr = reader.GetString(3).ToLower(),
                        Password = reader.GetString(4)
                    };
                    if (reader.GetString(4) == password)
                    {
                        reader.Close();
                        conn.Close();
                        return c;
                    }
                    else
                    {
                        reader.Close();
                        conn.Close();
                        return null;
                    }
                }
            }
            reader.Close();
            conn.Close();
            return null;
        }
        public Competitor AuthenticateCompetitorWithGmail(string email)
        {
            // only used for gmail login as only email is used to authenticate
            // DO NOT use when not using google login
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor
            WHERE Lower(EmailAddr) Like Lower(@selectedEmail)";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    Competitor c = new Competitor
                    {
                        CompetitorId = reader.GetInt32(0),
                        CompetitorName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        EmailAddr = reader.GetString(3).ToLower(),
                        Password = reader.GetString(4)
                    };
                    reader.Close();
                    conn.Close();
                    return c;
                }
            }
            reader.Close();
            conn.Close();
            return null;
        }
        public List<Competitor> GetAllCompetitors()
        {
            List<Competitor> cList = new List<Competitor>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competitor";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    Competitor c = new Competitor
                    {
                        CompetitorId = reader.GetInt32(0),
                        CompetitorName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        EmailAddr = reader.GetString(3).ToLower(),
                    };
                    cList.Add(c);
                }
            }
            reader.Close();
            conn.Close();
            return cList;
        }
    }
}
