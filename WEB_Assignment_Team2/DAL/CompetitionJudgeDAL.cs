﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CompetitionJudgeDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CompetitionJudgeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        //Method to get all competition related to the judge
        public List<CompetitionJudge> GetAllCompetition(int judgeid)
        {
            List<CompetitionJudge> listcompjudge = new List<CompetitionJudge>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionJudge where judgeid=@selectedid";
            cmd.Parameters.AddWithValue("@selectedid", judgeid);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    listcompjudge.Add(
                        new CompetitionJudge
                        {
                            CompetitionID = reader.GetInt32(0),
                            JudgeID = reader.GetInt32(1),
                        });
                }
            }
            reader.Close();
            conn.Close();
            return listcompjudge;
        }
        //Use to authenticate the right judge editing the right competition
        public bool AuthenticateCompJudge(int jid,int cid)
        {
            bool judgeFound = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT CompetitionID FROM CompetitionJudge WHERE JudgeID=@JudgeID";
            cmd.Parameters.AddWithValue("@JudgeID", jid);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) == cid)
                        judgeFound = true;
                }
            }
            else
            { //No record
                judgeFound = false;
            }
            reader.Close();
            conn.Close();
            return judgeFound;
        }
        
        // to assign judges for competition and insert to CompetitionJudge table
        public int AssignJudge(CompetitionJudge cj)
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO CompetitionJudge(CompetitionID, JudgeID)
                                VALUES(@competitionId, @judgeId)";
            cmd.Parameters.AddWithValue("@competitionId", cj.CompetitionID);
            cmd.Parameters.AddWithValue("@judgeId", cj.JudgeID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        } 

        // to remove judge from competition and delete from CompetitionJudge table
        public int RemoveCompJudge(int judgeId)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CompetitionJudge WHERE JudgeID = @selectJudgeID";
            cmd.Parameters.AddWithValue("@selectJudgeID", judgeId);

            conn.Open();
            int rowAffected = 0;

            rowAffected += cmd.ExecuteNonQuery();

            conn.Close();

            return rowAffected;
        }
    }
}
