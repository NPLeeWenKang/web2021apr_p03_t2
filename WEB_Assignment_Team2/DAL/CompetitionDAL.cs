﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CompetitionDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public Competition GetCompetition(int compid)
        {
            Competition targetComp = null;
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Competition Where CompetitionID = @selectcompid";
            cmd.Parameters.AddWithValue("@selectcompid", compid);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    targetComp = new Competition
                    {
                        CompetitionID = reader.GetInt32(0),
                        AreaInterestID = reader.GetInt32(1),
                        CompetitionName = reader.GetString(2),
                        StartDate = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null,
                        EndDate = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null,
                        ResultReleasedDate = !reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null,
                    };
                    
                }
            }
            reader.Close();
            conn.Close();
            return targetComp;
        }
        public List<Competition> GetAllCompetition()
        {
            // get all competitions
            List<Competition> complist = new List<Competition>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competition";
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    complist.Add(
                        new Competition
                        {
                            CompetitionID = reader.GetInt32(0),
                            AreaInterestID = reader.GetInt32(1),
                            CompetitionName = reader.GetString(2),
                            StartDate = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null,
                            EndDate = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null,
                            ResultReleasedDate = !reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null,
                        });
                }
            }
            reader.Close();
            conn.Close();
            return complist;
        }
        public List<Competition> GetJoinedCompetition(int id)
        {
            // gets all the competition the user has joined
            List<Competition> complist = new List<Competition>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competition c INNER JOIN CompetitionSubmission cs ON c.CompetitionID = cs.CompetitionID where cs.CompetitorID = @competitorID";
            cmd.Parameters.AddWithValue("@competitorID", id);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    complist.Add(
                        new Competition
                        {
                            CompetitionID = reader.GetInt32(0),
                            AreaInterestID = reader.GetInt32(1),
                            CompetitionName = reader.GetString(2),
                            StartDate = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null,
                            EndDate = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null,
                            ResultReleasedDate = !reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null,
                        });
                }
            }
            reader.Close();
            conn.Close();
            return complist;

        }
        public int AddCompetition(Competition competition)
        {
            SqlCommand cmd = conn.CreateCommand();
            //specify an INSERT SQL statement which will return auto-generated CompetitionID after insertion
            cmd.CommandText = @"INSERT INTO Competition (AreaInterestID, CompetitionName, StartDate, 
                                EndDate, ResultReleasedDate) 
                                OUTPUT INSERTED.CompetitionID 
                                VALUES(@areaInterestID, @competitionName, @startDate, @endDate, 
                                @resultReleasedDate)";

            cmd.Parameters.AddWithValue("@areaInterestID", competition.AreaInterestID);
            cmd.Parameters.AddWithValue("@competitionName", competition.CompetitionName);

            if (competition.StartDate == null)
                cmd.Parameters.AddWithValue("@startDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@startDate", competition.StartDate);
            if (competition.EndDate == null)
                cmd.Parameters.AddWithValue("@endDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@endDate", competition.EndDate);
            if (competition.ResultReleasedDate == null)
                cmd.Parameters.AddWithValue("@resultReleasedDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@resultReleasedDate", competition.ResultReleasedDate);

            conn.Open();
            //retrieve the auto-generated CompetitionID after executing the INSERT SQL statement
            competition.CompetitionID = (int)cmd.ExecuteScalar();

            conn.Close();
            //Return id when no error occurs.
            return competition.CompetitionID;
        }
        public int EditCompetition(Competition competition)
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"UPDATE Competition SET AreaInterestID = @areaInterestID, 
                                CompetitionName = @competitionName, StartDate = @startDate, 
                                EndDate = @endDate, ResultReleasedDate = @resultReleasedDate
                                WHERE CompetitionID = @selectedCompetitionID";

            cmd.Parameters.AddWithValue("@areaInterestID", competition.AreaInterestID);
            cmd.Parameters.AddWithValue("@competitionName", competition.CompetitionName);

            if (competition.StartDate == null)
                cmd.Parameters.AddWithValue("@startDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@startDate", competition.StartDate);
            if (competition.EndDate == null)
                cmd.Parameters.AddWithValue("@endDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@endDate", competition.EndDate);
            if (competition.ResultReleasedDate == null)
                cmd.Parameters.AddWithValue("@resultReleasedDate", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@resultReleasedDate", competition.ResultReleasedDate);

            cmd.Parameters.AddWithValue("@selectedCompetitionID", competition.CompetitionID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }

        // to check if any competitor has joined competition before editing & deleting
        public bool IsCompetitorExist(int competitionId)
        {
            bool competitorFound = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID=@selectedId";
            cmd.Parameters.AddWithValue("@selectedId", competitionId);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    competitorFound = true;
                }
            }
            else
            { //No record
                competitorFound = false;
            }
            reader.Close();
            conn.Close();
            return competitorFound;
        }
        public int DeleteCompetition(int competitionId)
        {
            SqlCommand cmd1 = conn.CreateCommand();
            cmd1.CommandText = @"DELETE FROM CompetitionJudge WHERE CompetitionID = @selectCompetitionID";
            cmd1.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            SqlCommand cmd2 = conn.CreateCommand();
            cmd2.CommandText = @"DELETE FROM CompetitionSubmission WHERE CompetitionID = @selectCompetitionID";
            cmd2.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            SqlCommand cmd3 = conn.CreateCommand();
            cmd3.CommandText = @"DELETE FROM Criteria WHERE CompetitionID = @selectCompetitionID";
            cmd3.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            SqlCommand cmd4 = conn.CreateCommand();
            cmd4.CommandText = @"DELETE FROM CompetitionScore WHERE CompetitionID = @selectCompetitionID";
            cmd4.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            SqlCommand cmd5 = conn.CreateCommand();
            cmd5.CommandText = @"DELETE FROM Comment WHERE CompetitionID = @selectCompetitionID";
            cmd5.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            SqlCommand cmd6 = conn.CreateCommand();
            cmd6.CommandText = @"DELETE FROM Competition WHERE CompetitionID = @selectCompetitionID";
            cmd6.Parameters.AddWithValue("@selectCompetitionID", competitionId);

            conn.Open();
            int rowAffected = 0;

            rowAffected += cmd1.ExecuteNonQuery();
            rowAffected += cmd2.ExecuteNonQuery();
            rowAffected += cmd3.ExecuteNonQuery();
            rowAffected += cmd4.ExecuteNonQuery();
            rowAffected += cmd5.ExecuteNonQuery();
            rowAffected += cmd6.ExecuteNonQuery();

            conn.Close();

            // Return number of row of Competition record updated or deleted
            return rowAffected;
        }
    }
}
