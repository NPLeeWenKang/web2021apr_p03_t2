﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2;
using WEB_Assignment_Team2.Models;
using System.Data;

namespace WEB_Assignment_Team2.DAL
{
    public class JudgeDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public JudgeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        //GetJudge is used to get judge details from database and returns Judge Object
        public Judge GetJudge(int judgeID)
        {
            Judge targetJudge = null;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge Where JudgeID = @selectJudgeID";
            cmd.Parameters.AddWithValue("@selectJudgeID", judgeID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    targetJudge = new Judge
                    {
                        JudgeID = reader.GetInt32(0),
                        JudgeName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        AreaInterestID = reader.GetInt32(3),
                        EmailAddr = reader.GetString(4).ToLower(),
                    };
                }
            }
            reader.Close();
            conn.Close();
            return targetJudge;
        }
        //Used to validate if Email is unique within the Judge database.
        private bool EmailValidate(string email)
        {
            bool emailFound;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge
            WHERE Lower(EmailAddr) = Lower(@selectedEmail)";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                emailFound = true;
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();
            return emailFound;
        }
        //Method to add judge into database
        public int AddJudge(Judge judge)
        {
            bool emailFound = EmailValidate(judge.EmailAddr);
            if (emailFound == true)
            {
                return -1;
            }
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Judge (JudgeName, Salutation, AreaInterestID, EmailAddr, Password)
            OUTPUT INSERTED.JudgeID
            VALUES(@name, @Salutation, @aoi, @email, @password)";
            cmd.Parameters.AddWithValue("@name", judge.JudgeName);
            if (judge.Salutation == "")
            {
                cmd.Parameters.AddWithValue("@salutation", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@salutation", judge.Salutation);
            }
            cmd.Parameters.AddWithValue("@aoi", judge.AreaInterestID);
            cmd.Parameters.AddWithValue("@email", judge.EmailAddr);
            cmd.Parameters.AddWithValue("@password", judge.Password);
            conn.Open();
            judge.JudgeID = (int)cmd.ExecuteScalar();
            conn.Close();
            return judge.JudgeID;
        }
        //Method to update judge's name and salutation
        public int UpdateProfile(Judge judge)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Judge SET JudgeName=@name,
Salutation=@salutation 
WHERE JudgeID = @selectedJudgeID";
            cmd.Parameters.AddWithValue("@name", judge.JudgeName);
            if (judge.Salutation == null)
            {
                cmd.Parameters.AddWithValue("@salutation", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@salutation", judge.Salutation);
            }
            cmd.Parameters.AddWithValue("@selectedJudgeID", judge.JudgeID);
            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();
            return count;
        }
        //Method to update Password. Authentication is in controller
        public int UpdatePassword(int id, string password)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Judge SET Password=@password
WHERE JudgeID = @selectedJudgeID";
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@selectedJudgeID", id);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }
        //Used to authenticate if it is the rightful account holder
        public Judge AuthenticateJudge(string loginEmail, string password)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge
            WHERE Lower(EmailAddr) = Lower(@selectedEmail)";
            cmd.Parameters.AddWithValue("@selectedEmail", loginEmail);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (password == reader.GetString(5))
                    {
                        Judge j = new Judge
                        {
                            JudgeID = reader.GetInt32(0),
                            JudgeName = reader.GetString(1),
                            Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                            AreaInterestID = reader.GetInt32(3),
                            EmailAddr = reader.GetString(4).ToLower(),
                        };
                        reader.Close();
                        conn.Close();
                        return j;
                    }
                }
            }
            reader.Close();
            conn.Close();
            return null;
        }
        //
        public List<Judge> GetJudgeInCompetition(int competitionId)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge j INNER JOIN CompetitionJudge cj ON j.JudgeID = cj.JudgeID WHERE cj.CompetitionID = @competitionID";
            cmd.Parameters.AddWithValue("@competitionID", competitionId);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Judge> jList = new List<Judge>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Judge j = new Judge
                    {
                        JudgeID = reader.GetInt32(0),
                        JudgeName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        AreaInterestID = reader.GetInt32(3),
                        EmailAddr = reader.GetString(4).ToLower(),
                    };
                    jList.Add(j);
                }
            }
            reader.Close();
            conn.Close();
            return jList;
        }

        public List<Judge> GetJudgeAOI(int compAOI)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge j WHERE AreaInterestID = @areaInterestId";
            cmd.Parameters.AddWithValue("@areaInterestId", compAOI);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Judge> jList = new List<Judge>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Judge j = new Judge
                    {
                        JudgeID = reader.GetInt32(0),
                        JudgeName = reader.GetString(1),
                        Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        AreaInterestID = reader.GetInt32(3),
                        EmailAddr = reader.GetString(4).ToLower(),
                    };
                    jList.Add(j);
                }
            }
            reader.Close();
            conn.Close();
            return jList;
        }
    }
}
