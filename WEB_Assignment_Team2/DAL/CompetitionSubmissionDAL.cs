﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CompetitionSubmissionDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CompetitionSubmissionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public List<CompetitionSubmission> GetAllSubmissions(int compid)
        {
            List<CompetitionSubmission> csl = new List<CompetitionSubmission>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission Where CompetitionID = @compid";
            cmd.Parameters.AddWithValue("@compid", compid);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    csl.Add(
                        new CompetitionSubmission
                        {
                            CompetitionID = reader.GetInt32(0),
                            CompetitorID = reader.GetInt32(1),
                            FileSubmitted = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                            DateTimeFileUpload = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null,
                            Appeal= !reader.IsDBNull(4) ? reader.GetString(4) : null,
                            VoteCount= reader.GetInt32(5),
                            Ranking = !reader.IsDBNull(6) ? reader.GetInt32(6) : (int?)null,
                        });
                }
            }
            reader.Close();
            conn.Close();
            return csl;
        }
        public CompetitionSubmission GetSubmission(int competitionId, int competitorId)
        {
            // gets a competitor's submission
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID = @competitionID AND CompetitorID = @competitorID";
            cmd.Parameters.AddWithValue("@competitionID", competitionId);
            cmd.Parameters.AddWithValue("@competitorID", competitorId);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    CompetitionSubmission cs = new CompetitionSubmission
                    {
                        CompetitionID = reader.GetInt32(0),
                        CompetitorID = reader.GetInt32(1),
                        FileSubmitted = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                        DateTimeFileUpload = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null,
                        Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : null,
                        VoteCount = reader.GetInt32(5),
                        Ranking = !reader.IsDBNull(6) ? reader.GetInt32(6) : (int?)null,
                    };
                    reader.Close();
                    conn.Close();
                    return cs;
                }
            }
            reader.Close();
            conn.Close();
            return null;
        }
        public void UpdateSubmissionFile(CompetitionSubmission cs)
        {
            // when competitor submits a file, updates the database's submission name
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET FileSubmitted=@fileName, DateTimeFileUpload=@dateTimeUpload WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            cmd.Parameters.AddWithValue("@fileName", cs.FileSubmitted);
            cmd.Parameters.AddWithValue("@dateTimeUpload", cs.DateTimeFileUpload);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", cs.CompetitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", cs.CompetitorID);
            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();
        }
        public bool IsJoined(int competitorId, int competitionId)
        {
            // checks whether competitor has join selected competition

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission Where CompetitorID = @competitorID AND CompetitionID = @competitionID";
            cmd.Parameters.AddWithValue("@competitorID", competitorId);
            cmd.Parameters.AddWithValue("@competitionID", competitionId);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Close DataReader
                reader.Close();
                //Close the database connection
                conn.Close();
                return true;
            }
            else
            {
                //Close DataReader
                reader.Close();
                //Close the database connection
                conn.Close();
                return false;
            }
        }
        public void AddCompetitionSubmission(int competitionID, int competitorID)
        {
            // Add an empty submission to database to signify that competitor has joined competition

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"INSERT INTO CompetitionSubmission (CompetitionID, CompetitorID, FileSubmitted, DateTimeFileUpload, Appeal, VoteCount, Ranking)
            VALUES(@CompetitionID, @CompetitorID, @FileSubmitted, @DateTimeFileUpload, @Appeal, @VoteCount, @Ranking)";
            cmd.Parameters.AddWithValue("@CompetitionID", competitionID);
            cmd.Parameters.AddWithValue("@CompetitorID", competitorID);
            cmd.Parameters.AddWithValue("@FileSubmitted", DBNull.Value);
            cmd.Parameters.AddWithValue("@DateTimeFileUpload", DBNull.Value);
            cmd.Parameters.AddWithValue("@Appeal", DBNull.Value);
            cmd.Parameters.AddWithValue("@VoteCount", 0);
            cmd.Parameters.AddWithValue("@Ranking", DBNull.Value);
            conn.Open();
            cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
        }
        public void AddAppeal(CompetitionSubmission cs)
        {
            // adds a competitor's appeal
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Appeal=@appeal WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            cmd.Parameters.AddWithValue("@appeal", cs.Appeal);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", cs.CompetitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", cs.CompetitorID);
            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();
        }
        private void RemoveDuplicateRank(int rank, int competitionID) {
            List<int> compidList = new List<int>();
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT CompetitorID FROM CompetitionSubmission Where Ranking = @rank AND CompetitionID = @competitionID";
            cmd.Parameters.AddWithValue("@rank", rank);
            cmd.Parameters.AddWithValue("@competitionID", competitionID);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    compidList.Add(reader.GetInt32(0));
                }
            }
            conn.Close();
            if (compidList.Count > 0)
            {
                foreach(int a in compidList)
                {
                cmd.CommandText = @"UPDATE CompetitionSubmission SET Ranking=@rankUpdate WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
                cmd.Parameters.AddWithValue("@rankUpdate", DBNull.Value);
                cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
                cmd.Parameters.AddWithValue("@selectedCompetitorID", a);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                }                
            }
        }
        public void UpdateRanking(int? rank,int competitionID,int competitorID)
        {
            if (rank != null)
            {
                RemoveDuplicateRank(rank.Value, competitionID);
            }
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Ranking=@rank WHERE CompetitionID = @selectedCompetitionID AND CompetitorID = @selectedCompetitorID";
            if (rank == null)
            {
                cmd.Parameters.AddWithValue("@rank", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@rank", rank.Value);
            }
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionID);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public void AddtoVote(string competitionId, string competitorId)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"Update CompetitionSubmission
Set VoteCount = VoteCount + 1
Where CompetitionID = @competionid AND CompetitorID = @competitorid";
            
            cmd.Parameters.AddWithValue("@competionid", competitionId);
            cmd.Parameters.AddWithValue("@competitorid", competitorId);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

     
    }
}
