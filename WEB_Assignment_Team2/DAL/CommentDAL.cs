﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CommentDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CommentDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Comment> GetCompComments(int competitionid)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Comment Where CompetitionID = @selectcompid";
            cmd.Parameters.AddWithValue("@selectcompid", competitionid);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a comment list
            List<Comment> commentList = new List<Comment>();
            while (reader.Read())
            {
                commentList.Add(
                new Comment
                {
                    CommentID = reader.GetInt32(0), //0: 1st column
                    CompetitionID = reader.GetInt32(1), //1: 2nd column
                    Description = reader.GetString(2), //2: 3rd column
                    DateTimePosted = reader.GetDateTime(3), //3: 4th column
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return commentList;
        }
        public int AddComment(Comment comment)
        {

            comment.DateTimePosted = DateTime.Now;
            SqlCommand cmd = conn.CreateCommand();

            //specify an INSERT SQL statement which will return auto-generated CompetitionID after insertion
            cmd.CommandText = @"INSERT INTO Comment (CompetitionID,Description, 
                                DateTimePosted)
                                OUTPUT INSERTED.CommentID
                                VALUES(@competitionid,@description, @datetimeposted)";

            cmd.Parameters.AddWithValue("@competitionid",comment.CompetitionID);
            cmd.Parameters.AddWithValue("@description", comment.Description);
            cmd.Parameters.AddWithValue("@datetimeposted", comment.DateTimePosted = DateTime.Now);


            conn.Open();
            //retrieve the auto-generated commentid after executing the INSERT SQL statement
            comment.CommentID = (int)cmd.ExecuteScalar();

            conn.Close();
            //Return id when no error occurs.
            return comment.CommentID;
        }
    }
}
