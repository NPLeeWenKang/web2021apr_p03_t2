﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CriteriaDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CriteriaDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Criteria> GetCriteria(int compid)
        {
            List<Criteria> criterialist = new List<Criteria>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Criteria where CompetitionID=@selectedid";
            cmd.Parameters.AddWithValue("@selectedid", compid);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    criterialist.Add(
                        new Criteria
                        {
                            CriteriaID = reader.GetInt32(0),
                            CompetitionID = reader.GetInt32(1),
                            CriteriaName = reader.GetString(2),
                            Weightage = reader.GetInt32(3),
                        });
                }
            }
            reader.Close();
            conn.Close();
            return criterialist;
        }
        public int AddCriterion(Criteria c)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Criteria (CompetitionID, criterianame,weightage)
            OUTPUT INSERTED.CriteriaID
            VALUES(@CompetitionID, @criterianame, @weightage)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@CompetitionID", c.CompetitionID);
            cmd.Parameters.AddWithValue("@criterianame", c.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", c.Weightage);
            //A connection to database must be opened before any operations made.
            conn.Open();
            c.CriteriaID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return c.CriteriaID;

        }
        public void UpdateCriterion(List<Criteria> cl, int compid)
        {
            List<int> criteriaList = new List<int>();
            List<int> removeList = new List<int>();
            foreach (Criteria c in cl)
            {
                criteriaList.Add(c.CriteriaID);
                Update(c);
            }
            List<Criteria>cList = GetCriteria(compid);
            foreach(Criteria c in cList)
            {
                bool inside = false;
                for(int i =0; i < criteriaList.Count(); i++)
                {
                    if (c.CriteriaID == criteriaList[i])
                    {
                        inside = true;
                        break;
                    }
                }
                if (inside == false)
                {
                    removeList.Add(c.CriteriaID);
                }
            }
            foreach(int i in removeList)
            {
                DeleteCriterion(i);
            }
        }
        private int DeleteCriterion(int criterionid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Criteria
                                WHERE CriteriaID = @criteriaID";
            cmd.Parameters.AddWithValue("@criteriaID", criterionid);
            conn.Open();
            int rowAffected = 0;
            rowAffected += cmd.ExecuteNonQuery();
            conn.Close();
            return rowAffected;
        }
        private void Update(Criteria c)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Criteria SET CompetitionID = @CompetitionID, CriteriaName = @criterianame,Weightage= @weightage
            WHERE CriteriaID = @selectedCriteriaID";
            cmd.Parameters.AddWithValue("@CompetitionID", c.CompetitionID);
            cmd.Parameters.AddWithValue("@criterianame", c.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", c.Weightage);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", c.CriteriaID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

    }
}
