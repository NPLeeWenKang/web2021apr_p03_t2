﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class AreaInterestDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public AreaInterestDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<AreaInterest> GetAllAreaInterest()
        {
            List<AreaInterest> ailist = new List<AreaInterest>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM AreaInterest";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    ailist.Add(
                        new AreaInterest
                        {
                            AreaInterestID = reader.GetInt32(0),
                            Name = reader.GetString(1),                            
                        });
                }
            }
            reader.Close();
            conn.Close();
            return ailist;
        }

        public int AddAI(AreaInterest areaInterest)
        {
            SqlCommand cmd = conn.CreateCommand();
            //specify an INSERT SQL statement which will return auto-generated AreaInterestID after insertion
            cmd.CommandText = @"INSERT INTO AreaInterest (Name) 
                                OUTPUT INSERTED.AreaInterestID 
                                VALUES(@name)";
            cmd.Parameters.AddWithValue("@name", areaInterest.Name);
            conn.Open();
            //retrieve the auto-generated AreaInterestID after executing the INSERT SQL statement
            areaInterest.AreaInterestID = (int)cmd.ExecuteScalar();
            conn.Close();
            return areaInterest.AreaInterestID;
        }

        // to check if name exists before creating area of interest
        public bool IsNameExist(string name, int areaInterestId)
        {
            bool nameFound = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT AreaInterestID FROM AreaInterest WHERE Name=@selectedName";
            cmd.Parameters.AddWithValue("@selectedName", name);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != areaInterestId)
                        nameFound = true;
                }
            }
            else
            { //No record
                nameFound = false; // The name given does not exist
            }
            reader.Close();
            conn.Close();
            return nameFound;
        }

        // to check if any competitions is associated to AreaInterest before deleting
        public bool IsCompetitionExist(int areaInterestId)
        {
            bool competitionFound = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competition WHERE AreaInterestID=@selectedId";
            cmd.Parameters.AddWithValue("@selectedId", areaInterestId);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    competitionFound = true;
                }
            }
            else
            { //No record
                competitionFound = false;
            }
            reader.Close();
            conn.Close();
            return competitionFound;
        }

        // to check if any judges is associated to AreaInterest before deleting
        public bool IsJudgeExist(int areaInterestId)
        {
            bool judgeFound = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Judge WHERE AreaInterestID=@selectedId";
            cmd.Parameters.AddWithValue("@selectedId", areaInterestId);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    judgeFound = true;
                }
            }
            else
            { //No record
                judgeFound = false;
            }
            reader.Close();
            conn.Close();
            return judgeFound;
        }

        public int Delete(int areaInterestID)
        {
            SqlCommand cmd1 = conn.CreateCommand();
            cmd1.CommandText = @"DELETE FROM Competition WHERE AreaInterestID = @selectAreaInterestID";
            cmd1.Parameters.AddWithValue("@selectAreaInterestID", areaInterestID);

            SqlCommand cmd2 = conn.CreateCommand();
            cmd2.CommandText = @"DELETE FROM AreaInterest WHERE AreaInterestID = @selectAreaInterestID";
            cmd2.Parameters.AddWithValue("@selectAreaInterestID", areaInterestID);

            conn.Open();

            int rowAffected = 0;

            rowAffected += cmd1.ExecuteNonQuery();
            rowAffected += cmd2.ExecuteNonQuery();

            conn.Close();

            // Return number of row of AreaInterest record updated or deleted
            return rowAffected;
        }
    }
}
