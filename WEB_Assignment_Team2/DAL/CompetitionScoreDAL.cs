﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB_Assignment_Team2;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.DAL
{
    public class CompetitionScoreDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CompetitionScoreDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CompetifyConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public List<CompetitionScore> GetScores(int competitionID, int competitorID)
        {
            List<CompetitionScore> sList = new List<CompetitionScore>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionScore where CompetitorID=@competitorID AND CompetitionID=@competitionID";
            cmd.Parameters.AddWithValue("@competitorID", competitorID);
            cmd.Parameters.AddWithValue("@competitionID", competitionID);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    sList.Add(
                        new CompetitionScore
                        {
                            CriteriaID= reader.GetInt32(0),
                            CompetitorID = reader.GetInt32(1),
                            CompetitionID = reader.GetInt32(2),
                            Score = reader.GetInt32(3), 
                        }
                        );
                }
            }
            reader.Close();
            conn.Close();
            return sList;
        }
        public List<CriteriaScore> GetCriteriaScores(int competitionID, int competitorID)
        {
            // gets gets a competitor's score for the selected competition
            List<CriteriaScore> csList = new List<CriteriaScore>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT cs.CriteriaID, cs.CompetitionID,cs.CompetitorID, c.CriteriaName, cs.Score FROM CompetitionScore cs INNER JOIN Criteria c ON cs.CriteriaID = c.CriteriaID WHERE cs.CompetitionID = @competitionID AND cs.CompetitorID = @competitorID";

            cmd.Parameters.AddWithValue("@competitorID", competitorID);
            cmd.Parameters.AddWithValue("@competitionID", competitionID);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    csList.Add(new CriteriaScore { CriteriaID = reader.GetInt32(0),
                        CompetitionID = reader.GetInt32(1),
                        CompetitorID = reader.GetInt32(2),
                        CriteriaName = reader.GetString(3),
                        Score = reader.GetInt32(4)
                    });
                }
            }
            reader.Close();
            conn.Close();
            return csList;
        }
        public void InitializeScore(CompetitionScore cscore)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO CompetitionScore (CriteriaID,CompetitorID,CompetitionID,Score)
            VALUES(@CriteriaID, @CompetitorID, @CompetitionID, @Score)";
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@CriteriaID", cscore.CriteriaID);
            cmd.Parameters.AddWithValue("@CompetitorID", cscore.CompetitorID);
            cmd.Parameters.AddWithValue("@CompetitionID", cscore.CompetitionID);
            cmd.Parameters.AddWithValue("@Score", cscore.Score);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
        }
        public void updateScore(CompetitionScore cscore)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionScore SET score = @score
            WHERE CriteriaID = @selectedCriteriaID and CompetitionID = @competitionid and competitorID=@competitorID";
            cmd.Parameters.AddWithValue("@score", cscore.Score);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", cscore.CriteriaID);
            cmd.Parameters.AddWithValue("@competitionid", cscore.CompetitionID);
            cmd.Parameters.AddWithValue("@competitorID", cscore.CompetitorID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
