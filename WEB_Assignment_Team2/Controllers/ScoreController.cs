﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;
using Microsoft.AspNetCore.Http;

namespace WEB_Assignment_Team2.Controllers
{
    public class ScoreController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitionScoreDAL scoreContext = new CompetitionScoreDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();
        private CompetitionSubmissionDAL compSubmissionContext = new CompetitionSubmissionDAL();
        private CriteriaDAL criteriaDAL = new CriteriaDAL();

        private void SetAOI(Competition c)
        {
            List<AreaInterest> ailist = aiContext.GetAllAreaInterest();
            ViewData["AreaOfInterest"] = ailist.Find(a => a.AreaInterestID == c.AreaInterestID).Name;
        }
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }
            ViewData["tab"] = "criteria";
            ViewData["id"] = "" + id;

            if ((HttpContext.Session.GetString("Role") != null) &&
            (HttpContext.Session.GetString("Role") == "competitor"))
            {
                // check whether to display join button
                if (compSubmissionContext.IsJoined(Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")), id.Value))
                {
                    ViewData["DisplayJoinButton"] = false;
                }
                else
                {
                    ViewData["DisplayJoinButton"] = true;
                }

                CompetitionScoreViewModel cs = new CompetitionScoreViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    CompSubmission = compSubmissionContext.GetSubmission(id.Value, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID"))),
                    CriteriaScoreList = scoreContext.GetCriteriaScores(id.Value, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")))
                };

                // Check whether competition exists
                if (cs.SelectedCompetition == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                if (cs.CompSubmission != null)
                {
                    if (String.IsNullOrWhiteSpace(cs.CompSubmission.Appeal)) // if user has already submitted appeal, dont allow them to re-submit
                    {
                        ViewData["AllowAppeal"] = "true";
                    }
                    else
                    {
                        ViewData["AllowAppeal"] = "false";
                    }
                }
                
                // Set Area of Interest in _CompetitionDetails
                SetAOI(cs.SelectedCompetition);
                return View(cs);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitAppeal(CompetitionScoreViewModel cs)
        {
            // whether to submit appeal
            if (!String.IsNullOrWhiteSpace(cs.CompSubmission.Appeal))
            {
                compSubmissionContext.AddAppeal(cs.CompSubmission);
            }
            return RedirectToAction("View", "Score", new { id = cs.CompSubmission.CompetitionID});
        }
    }
}
