﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class CriteriaController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();
        private CompetitionSubmissionDAL compSubmissionContext = new CompetitionSubmissionDAL();
        private void SetAOI(Competition c)
        {
            //Populate list in view
            List<AreaInterest> ailist = aiContext.GetAllAreaInterest();
            ViewData["AreaOfInterest"] = ailist.Find(a => a.AreaInterestID == c.AreaInterestID).Name;
        }
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }
            ViewData["tab"] = "criteria";
            ViewData["id"] = "" + id;

            CriteriaViewModel cc = new CriteriaViewModel
            {
                SelectedCompetition = competitionContext.GetCompetition(id.Value),
                CriteriaList = criteriaContext.GetCriteria(id.Value),
            };

            // Check whether competition exists
            if (cc.SelectedCompetition == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }

            // Set Area of Interest in _CompetitionDetails
            SetAOI(cc.SelectedCompetition);

            if (HttpContext.Session.GetString("Role") == "judge" &&
            HttpContext.Session.GetString("JudgeID") != null)
            {
                if (cc.SelectedCompetition == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                return View(cc);
            }
            else if ((HttpContext.Session.GetString("Role") != null) &&
            (HttpContext.Session.GetString("Role") == "competitor"))
            {
                // checks whether competitor has joined competition, and if join button needs to be shown
                if (compSubmissionContext.IsJoined(Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")), id.Value))
                {
                    ViewData["DisplayJoinButton"] = false;
                } else
                {
                    ViewData["DisplayJoinButton"] = true;
                }

                return View(cc);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult Edit(int? id)
        {
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (id == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                CriteriaViewModel cc = new CriteriaViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    CriteriaList = criteriaContext.GetCriteria(id.Value),
                    Command = "false",
                };
                //Set Area of interest
                SetAOI(cc.SelectedCompetition);
                //Check if competition exits and that it is the valid time period to edit criteria,
                // check if the judge editing the criteria is assigned that competition
                if (cc.SelectedCompetition == null || cc.SelectedCompetition.StartDate < DateTime.Now ||
                    !compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cc.SelectedCompetition.CompetitionID))
                {
                    return RedirectToAction("View", new { id = id.Value });
                }
                return View(cc);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        [HttpPost]
        public ActionResult Edit(CriteriaViewModel cc)
        {
            //Validation
            if (HttpContext.Session.GetString("Role") != "judge")
            {
                return RedirectToAction("index", "home");
            }
            if (cc.SelectedCompetition == null || cc.SelectedCompetition.StartDate < DateTime.Now ||
                    !compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cc.SelectedCompetition.CompetitionID))
            {
                return RedirectToAction("View", new { id = cc.SelectedCompetition.CompetitionID });
            }
            //Removing away criteria with 0 weightage
            List<Criteria> newCList = new List<Criteria>();
            SetAOI(cc.SelectedCompetition);
            List<string> nameList = new List<string>();
            nameList.Add("");
            //Checking if all names are unique
            foreach (Criteria c in cc.CriteriaList)
            {
                bool repeat = false;
                foreach (string name in nameList)
                {
                    if (name.ToLower() == c.CriteriaName.ToLower())
                    {
                        repeat = true;
                        c.Weightage = 0;
                        break;
                    }
                }
                if (repeat == false)
                {
                    nameList.Add(c.CriteriaName);
                }
            }
            foreach(Criteria c in cc.CriteriaList)
            {
                if (c.Weightage > 0 && c.Weightage <= 100)
                {
                    newCList.Add(c);
                }
            }
            ModelState.Clear();
            cc.CriteriaList = newCList;
            if(cc.Command == "true")
            {
                Criteria newC = new Criteria
                {
                    CompetitionID = cc.SelectedCompetition.CompetitionID,
                    CriteriaName = "",
                    Weightage = 0,
                };
                newCList.Add(newC);
                cc.Command = "false";
            }
            else if(cc.Command == "Save")
            {
                int score = 0;
                foreach(Criteria ccc in cc.CriteriaList)
                {
                    score += ccc.Weightage;
                }
                if (score!= 100)
                {
                    TempData["MessageView"] = "Weightage does not add up to 100!";
                    return View(cc);

                }
                else
                {
                    List<Criteria> cWithCID = new List<Criteria>();
                    List<Criteria> cWOCID = new List<Criteria>();
                    foreach (Criteria ccc in cc.CriteriaList)
                    {
                        if (ccc.CriteriaID > 0)
                        {
                            cWithCID.Add(ccc);
                        }
                        else
                        {
                            cWOCID.Add(ccc);
                        }
                    }
                    criteriaContext.UpdateCriterion(cWithCID, cc.SelectedCompetition.CompetitionID);
                    foreach(Criteria criteria in cWOCID)
                    {
                        criteriaContext.AddCriterion(criteria);
                    }
                    
                }
                return RedirectToAction("View", new { id = cc.SelectedCompetition.CompetitionID });
            }
            return View(cc);
        }        
    }    
}
