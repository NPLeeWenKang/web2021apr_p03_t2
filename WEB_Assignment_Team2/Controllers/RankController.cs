﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class RankController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();
        private CompetitionSubmissionDAL compSubmissionContext = new CompetitionSubmissionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionScoreDAL competitionScoreContext = new CompetitionScoreDAL();
        private List<SelectListItem> rankinglist = new List<SelectListItem>();
        //Send area of interest back to view
        private void SetAOI(Competition c)
        {
            List<AreaInterest> ailist = aiContext.GetAllAreaInterest();
            ViewData["AreaOfInterest"] = ailist.Find(a => a.AreaInterestID == c.AreaInterestID).Name;
        }
        private void SetRank(int? compRank, int noOfCompetitor)
        {
            //Populate list
            // Adding Ranks into a list
            List<int?> rank = new List<int?> { null};
            for(int i =0; i < noOfCompetitor; i++)
            {
                rank.Add(i + 1);
            }
            foreach(int? a in rank)
            {
                if(compRank == a)
                {
                    rankinglist.Add(new SelectListItem { Value = Convert.ToString(a), Text = Convert.ToString(a), Selected = true });
                }
                else
                {
                    rankinglist.Add(new SelectListItem { Value = Convert.ToString(a), Text = Convert.ToString(a), Selected = false });
                }
            }
            ViewData["rank"] = rankinglist;
        }
        //Method to set model
        private RankViewModel SetModel(int competitionID, int competitorID, Competition c,CompetitionSubmission cs,bool edit)
        {
            SubmissionViewModel csl = new SubmissionViewModel
            {
                SelectedCompetition = c,
                CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(c.CompetitionID).OrderBy(i => i.Ranking).ToList(),
                CompetitorList = competitorContext.GetAllCompetitors(),
            };
            List<CompetitionScore> cscore = competitionScoreContext.GetScores(competitionID, competitorID);
            List<Criteria> cList = criteriaContext.GetCriteria(competitionID);
            SubmissionScoreViewModel ssvm = new SubmissionScoreViewModel
            {
                CompSub = cs,
                CriteriaList = cList,
                CsList = cscore,
            };
            int score = 0;

            //Calculate Score
            for (int a = 0; a < cscore.Count; a++)
            {
                score += cscore[a].Score * cList.Find(b => b.CriteriaID == cscore[a].CriteriaID).Weightage;
            }
            List<CompetitionSubmission> csRemove = new List<CompetitionSubmission>();
            foreach (CompetitionSubmission ccs in csl.CompetitionSubmissionList)
            {
                if (ccs.DateTimeFileUpload == null || ccs.FileSubmitted == null)
                {
                    csRemove.Add(ccs);
                }
            }
            foreach (CompetitionSubmission ccs in csRemove)
            {
                csl.CompetitionSubmissionList.Remove(ccs);
            }
            RankViewModel rvm = new RankViewModel
            {
                SubmissionScoreVM = ssvm,
                SubmissionVM = csl,
                TotalScore = score / 10.0,
            };
            //if true
            if (edit)
            {
                SetRank(ssvm.CompSub.Ranking, csl.CompetitionSubmissionList.Count);
            }            
            return rvm;
        }
        public ActionResult List(int? id)
        {
            //Validation
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (id == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition selectedCompetition = competitionContext.GetCompetition(id.Value);
                SetAOI(selectedCompetition);
                if (selectedCompetition == null ||
                    !compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), selectedCompetition.CompetitionID))
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                //Preparing Model to be sent to view
                SubmissionViewModel csl = new SubmissionViewModel
                {
                    SelectedCompetition = selectedCompetition,
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(id.Value).OrderBy(i => i.Ranking).ToList(),
                    CompetitorList = competitorContext.GetAllCompetitors(),
                };
                List<CompetitionSubmission> csRemove = new List<CompetitionSubmission>();
                foreach (CompetitionSubmission cs in csl.CompetitionSubmissionList)
                {
                    if (cs.DateTimeFileUpload == null || cs.FileSubmitted == null)
                    {
                        csRemove.Add(cs);
                    }
                }
                foreach (CompetitionSubmission cs in csRemove)
                {
                    csl.CompetitionSubmissionList.Remove(cs);
                }
                return View(csl);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
       // public to view ranks
        public ActionResult PublicRankView(int? id)
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                PublicViewModel ps = new PublicViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(id.Value).OrderBy(i => i.Ranking).ToList(),
                    competitorList = competitorContext.GetAllCompetitors(),
                };
                SetAOI(ps.SelectedCompetition);
                return View(ps);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult View(int? competitionID, int? competitorID)
        {
            //Validate
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (competitionID == null || competitorID == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                if (!compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cs.CompetitionID)
                    || cs == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition c = competitionContext.GetCompetition(competitionID.Value);
                if (c.ResultReleasedDate > DateTime.Now && c.EndDate < DateTime.Now)
                {
                    TempData["Validity"] = "true";
                }
                else
                {
                    TempData["Validity"] = "false";
                }
                //Set model to be sent to view
                RankViewModel rvm = SetModel(competitionID.Value,competitorID.Value,c,cs,false);
                if(rvm == null)
                {
                    return RedirectToAction("CompetitorSubmission", "Submission", new { competitionID = competitionID.Value,competitorID = competitorID.Value  });
                }
                return View(rvm);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult Edit(int? competitionID, int? competitorID)
        {
            //Validation
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (competitionID == null || competitorID == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                if (!compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cs.CompetitionID)
                    || cs == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition c = competitionContext.GetCompetition(competitionID.Value);
                if (c.ResultReleasedDate < DateTime.Now || c.EndDate > DateTime.Now)
                {
                    return RedirectToAction("View", "Rank", new { competitionID = competitionID.Value, competitorID = competitorID.Value });
                }
                //prepare model to be sent to view
                RankViewModel rvm = SetModel(competitionID.Value, competitorID.Value, c, cs,true);
                if (rvm == null)
                {
                    return RedirectToAction("CompetitorSubmission", "Submission", new { competitionID = competitionID.Value, competitorID = competitorID.Value });
                }
                return View(rvm);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RankViewModel rvm)
        {
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                
                int competitionID = rvm.SubmissionScoreVM.CompSub.CompetitionID;
                int competitorID = rvm.SubmissionScoreVM.CompSub.CompetitorID;
                //update ranking
                compSubmissionContext.UpdateRanking(rvm.SubmissionScoreVM.CompSub.Ranking, competitionID, competitorID);
                //Prepare model to be send to view
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID, competitorID);
                SubmissionViewModel csl = new SubmissionViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(competitionID),
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(competitionID).OrderBy(i => i.Ranking).ToList(),
                    CompetitorList = competitorContext.GetAllCompetitors(),
                };
                List<CompetitionScore> cscore = competitionScoreContext.GetScores(competitionID, competitorID);
                List<Criteria> cList = criteriaContext.GetCriteria(competitionID);
                SubmissionScoreViewModel ssvm = new SubmissionScoreViewModel
                {
                    CompSub = cs,
                    CriteriaList = cList,
                    CsList = cscore,
                };
                rvm.SubmissionScoreVM = ssvm;
                rvm.SubmissionVM = csl;
                SetRank(ssvm.CompSub.Ranking, csl.CompetitionSubmissionList.Count);
                return RedirectToAction("View","Rank",new { competitionID = competitionID, competitorID = competitorID });
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
    }
}
