﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class CompetitionController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();
        private CompetitionSubmissionDAL competitionSubmissionCOntext = new CompetitionSubmissionDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private JudgeDAL judgeContext = new JudgeDAL();
        public ActionResult MyCompetition()
        {
            //Authenticate user session state
            if ((HttpContext.Session.GetString("Role") == "judge" &&
            HttpContext.Session.GetString("JudgeID") != null))
            {
                List<CompetitionJudge> listcompjudge = compJudgeContext.GetAllCompetition(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")));
                List<Competition> listCompetition = new List<Competition>();
                //get all competition details from database
                foreach (CompetitionJudge cj in listcompjudge)
                {
                    listCompetition.Add(competitionContext.GetCompetition(cj.CompetitionID));
                }
                //Prepare model to be sent to view
                CompetitionViewModel compList = new CompetitionViewModel
                {
                    ListCompetition = listCompetition,
                    AiList = aiContext.GetAllAreaInterest(),
                };
                return View(compList);
            }
            else if ((HttpContext.Session.GetString("Role") != null) &&
            (HttpContext.Session.GetString("Role") == "competitor"))
            {
                // Getting all the competitions the user has joined
                CompetitionViewModel compList = new CompetitionViewModel
                {
                    ListCompetition = competitionContext.GetJoinedCompetition(Convert.ToInt32(HttpContext.Session.GetString("CompetitorID"))),
                    AiList = aiContext.GetAllAreaInterest(),
                };
                return View(compList);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult AllCompetitions()
        {
            // gets all the competitions in the database
            CompetitionViewModel compList = new CompetitionViewModel
            {
                ListCompetition = competitionContext.GetAllCompetition(),
                AiList = aiContext.GetAllAreaInterest(),
            };
            return View(compList);
        }

        // view all competitions and judges assigned
        public ActionResult CompetitionJudges(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "admin"))
            {
                return RedirectToAction("index", "home");
            }
            CompetitionViewModel compList = new CompetitionViewModel
            {
                ListCompetition = competitionContext.GetAllCompetition(),
                AiList = aiContext.GetAllAreaInterest(),
            };
            ViewData["selComp"] = "";
            if (id != null)
            {
                CompetitionJudgeViewModel compjudge = new CompetitionJudgeViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    JudgeList = judgeContext.GetJudgeInCompetition(id.Value),
                };
                //check if after end date of competition
                if (DateTime.Now > compjudge.SelectedCompetition.EndDate)
                {
                    // Display manage judges error message
                    TempData["CompJudgeError"] = "Competition have ended, unable to manage judges.";
                    return View(compList);
                }
                //display message according to number of judges assigned to competition
                if (compjudge.JudgeList.Count == 0)
                    TempData["CompJudgeError"] = "There are currently no judges assigned to " + compjudge.SelectedCompetition.CompetitionName + ". Please assign at least two judges!";
                else
                    TempData["CompJudgeSuccess"] = "Judges in " + compjudge.SelectedCompetition.CompetitionName + " have been displayed.";
                ViewData["selComp"] = id.Value;
                compList.CompJudgeVM = compjudge;
            }
            return View(compList);
        }

        public ActionResult CompetitionRanking()
        {
            CompetitionViewModel compList = new CompetitionViewModel
            {
                ListCompetition = competitionContext.GetAllCompetition(),
                AiList = aiContext.GetAllAreaInterest(),
            };
            return View(compList);
        }
        public ActionResult JoinCompetition(int competitionID)
        {
            // adds an empty submission into competition table
            // makes competitor join the competition
            if ((HttpContext.Session.GetString("Role") == "competitor" &&
            HttpContext.Session.GetString("CompetitorID") != null))
            {
                Competition c = competitionContext.GetCompetition(competitionID);
                if (c != null)
                {
                    if (c.StartDate > DateTime.Now.AddDays(3))
                    {
                        competitionSubmissionCOntext.AddCompetitionSubmission(competitionID, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")));
                    }

                }
                
            }
            return RedirectToAction("View", "Criteria", new { id = competitionID });
        }

        public ActionResult CreateCompetition()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "admin"))
            {
                return RedirectToAction("index", "home");
            }
            // Get list of AreaInterest to display as dropdown list
            ViewData["AIList"] = GetAllAI();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCompetition(Competition competition)
        {
            // Get AreaInterest list in case need to return to CreateCompetition page
            ViewData["AIList"] = GetAllAI();
            if (ModelState.IsValid)
            {
                // check start and end dates of competition
                if (competition.StartDate > competition.EndDate)
                {
                    TempData["CCError"] = "Competition start date cannot be later than end date!";
                    return View(competition);
                }
                // check end and result release dates of competition
                if (competition.EndDate >= competition.ResultReleasedDate)
                {
                    TempData["CCError"] = "Competition end date cannot be later than result released date!";
                    return View(competition);
                }
                competition.CompetitionID = competitionContext.AddCompetition(competition);
                // Display create competition success message
                TempData["CompSuccess"] = "New Competition successfully created!";
                return RedirectToAction("AllCompetitions");
            }
            else
            {
                return View(competition);
            }
        }
        public ActionResult EditCompetition(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { 
                return RedirectToAction("AllCompetitions");
            }
            bool compExist = competitionContext.IsCompetitorExist(id.Value);
            if (compExist == true) //competitor records found
            {
                // Display edit competition error message
                TempData["CompError"] = "There are competitor records found, competition cannot be edited.";
                return RedirectToAction("AllCompetitions");
            }
            ViewData["AIList"] = GetAllAI();
            Competition competition = competitionContext.GetCompetition(id.Value);
            if (competition == null)
            {
                // Return to listing page, not allowed to edit
                return RedirectToAction("AllCompetitions");
            }
            return View(competition);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCompetition(Competition competition)
        {
            ViewData["AIList"] = GetAllAI();
            if (ModelState.IsValid)
            {
                // check start and end dates of competition
                if (competition.StartDate > competition.EndDate)
                {
                    TempData["ECError"] = "Competition start date cannot be later than end date!";
                    return View(competition);
                }
                // check end and result release dates of competition
                if (competition.EndDate >= competition.ResultReleasedDate)
                {
                    TempData["ECError"] = "Competition end date cannot be the same or later than result released date!";
                    return View(competition);
                }
                competitionContext.EditCompetition(competition);
                // Display edit competition success message
                TempData["CompSuccess"] = "Details for " + competition.CompetitionName + " successfully edited!";
                return RedirectToAction("AllCompetitions");
            }
            else
            {
                // Input validation fails, return to the view to display error message
                return View(competition);
            }
        }
        public ActionResult DeleteCompetition(int? id)
        {
            if (HttpContext.Session.GetString("Role") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            bool compExist = competitionContext.IsCompetitorExist(id.Value);
            if (compExist == true) //competitor records found
            {
                // Display delete competition error message
                TempData["CompError"] = "There are competitor records found, competition cannot be deleted.";
                return RedirectToAction("AllCompetitions");
            }
            else
            {
                competitionContext.DeleteCompetition(id.Value);
                // Display delete competition success message
                TempData["CompSuccess"] = "Competition successfully deleted!";
                return RedirectToAction("AllCompetitions");
            }
        }

        private List<AreaInterest> GetAllAI()
        {
            // Get a list of Area of Interest from database
            List<AreaInterest> aiList = aiContext.GetAllAreaInterest();
            return aiList;
        }
    }
}
