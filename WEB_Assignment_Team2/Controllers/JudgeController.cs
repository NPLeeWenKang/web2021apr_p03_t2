﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class JudgeController : Controller
    {
        private JudgeDAL judgeContext = new JudgeDAL();
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionSubmissionDAL compSubmissionContext = new CompetitionSubmissionDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();


        private void SetAOI(Competition c)
        {
            List<AreaInterest> ailist = aiContext.GetAllAreaInterest();
            ViewData["AreaOfInterest"] = ailist.Find(a => a.AreaInterestID == c.AreaInterestID).Name;
        }
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }
            CompetitionJudgeViewModel cc = new CompetitionJudgeViewModel
            {
                SelectedCompetition = competitionContext.GetCompetition(id.Value),
                JudgeList = judgeContext.GetJudgeInCompetition(id.Value),
            };
            // Check whether competition exists
            if (cc.SelectedCompetition == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }
            // Set up Area of Interest in _CompetitionDetails
            SetAOI(cc.SelectedCompetition);

            if ((HttpContext.Session.GetString("Role") != null) &&
            (HttpContext.Session.GetString("Role") == "competitor"))
            {
                // checks whether competitor has joined competition, and if join button needs to be shown
                if (compSubmissionContext.IsJoined(Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")), id.Value))
                {
                    ViewData["DisplayJoinButton"] = false;
                }
                else
                {
                    ViewData["DisplayJoinButton"] = true;
                }
                return View(cc);
            }
            else if (HttpContext.Session.GetString("Role") == "judge")
            {
                return View(cc);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        // assign judge to selected competition 
        public ActionResult AssignJudge(int? id)
        {
            CompetitionJudgeViewModel compjudge = new CompetitionJudgeViewModel
            {
                SelectedCompetition = competitionContext.GetCompetition(id.Value),
                JudgeList = judgeContext.GetJudgeInCompetition(id.Value),
            };
            string CompName = compjudge.SelectedCompetition.CompetitionName;
            ViewData["CompName"] = CompName;
            ViewData["CompId"] = compjudge.SelectedCompetition.CompetitionID;
            int CompAOI = compjudge.SelectedCompetition.AreaInterestID;

            // check and remove judges already assigned to competition
            List<Judge> jList = judgeContext.GetJudgeAOI(CompAOI);
            foreach (Judge cj in compjudge.JudgeList)
            {
                for (int i = 0; i < jList.Count; i++)
                {
                    if (jList[i].JudgeID == cj.JudgeID)
                    {
                        jList.Remove(jList[i]);
                    }
                }
            }
            // if no more judges available for selected cometition 
            if (jList.Count == 0)
            {
                TempData["CompJudgeError"] = "No more judges available for " + CompName + "!";
                return RedirectToAction("CompetitionJudges", "Competition");
            }
            ViewData["JudgeList"] = jList;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignJudge(CompetitionJudge competitionJudge)
        {
            CompetitionJudgeViewModel compjudge = new CompetitionJudgeViewModel
            {
                SelectedCompetition = competitionContext.GetCompetition(competitionJudge.CompetitionID),
                JudgeList = judgeContext.GetJudgeInCompetition(competitionJudge.CompetitionID),
            };
            string CompName = compjudge.SelectedCompetition.CompetitionName;
            ViewData["CompName"] = CompName;
            int CompId = compjudge.SelectedCompetition.CompetitionID;
            ViewData["CompId"] = CompId;
            int CompAOI = compjudge.SelectedCompetition.AreaInterestID;

            // check and remove judges already assigned to competition
            List<Judge> jList = judgeContext.GetJudgeAOI(CompAOI);
            foreach (Judge cj in compjudge.JudgeList)
            {
                for (int i = 0; i < jList.Count; i++)
                {
                    if (jList[i].JudgeID == cj.JudgeID)
                    {
                        jList.Remove(jList[i]);
                    }
                }
            }
            ViewData["JudgeList"] = jList;
            if (ModelState.IsValid)
            {
                // check if selected judge is involved in current or future competitions
                List<CompetitionJudge> cjList = compJudgeContext.GetAllCompetition(competitionJudge.JudgeID);
                foreach (CompetitionJudge cJudge in cjList)
                {
                    if (cJudge.CompetitionID != CompId) // exclude competition judge is going to be assigned to
                    {
                        Competition c = competitionContext.GetCompetition(cJudge.CompetitionID);
                        // using current date as reference
                        if (c.StartDate >= DateTime.Now && c.EndDate >= DateTime.Now)
                        {
                            // judge involved in other current or future competitions
                            Judge j = judgeContext.GetJudge(competitionJudge.JudgeID);
                            TempData["AssignJudgeError"] = j.JudgeName + " is involved in a current or future competition. Please select another judge.";
                            return View(competitionJudge);
                        }
                    }
                }
                compJudgeContext.AssignJudge(competitionJudge);
                TempData["CompJudgeSuccess"] = "New judge successfully assigned to " + CompName + "!";
                return RedirectToAction("CompetitionJudges", "Competition");
            }
            else
            {
                return View(competitionJudge);
            }
        }

        // to remove selected judge from competition
        public ActionResult RemoveJudge(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "admin"))
            {
                return RedirectToAction("index", "home");
            }
            if (id == null)
            {
                return RedirectToAction("CompetitionJudges", "Competition");
            }
            Judge judge = judgeContext.GetJudge(id.Value);
            if (judge == null)
            {
                TempData["CompJudgeError"] = "Error! No judge found.";
                return RedirectToAction("CompetitionJudges", "Competition");
            }
            compJudgeContext.RemoveCompJudge(id.Value);
            TempData["CompJudgeSuccess"] = "Judge removed from competition successfully!";
            return RedirectToAction("CompetitionJudges", "Competition");
        }
    }
}
