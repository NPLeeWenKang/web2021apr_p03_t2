﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class AreaInterestController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") != "admin")
            {
                return RedirectToAction("index", "home");
            }
            List<AreaInterest> aiList = aiContext.GetAllAreaInterest();
            return View(aiList);
        }

        public ActionResult Create()
        {
            if (HttpContext.Session.GetString("Role") != "admin")
            {
                return RedirectToAction("index", "home");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AreaInterest areaInterest)
        {
            if (ModelState.IsValid)
            {
                areaInterest.AreaInterestID = aiContext.AddAI(areaInterest);
                // Display create area of interest success message
                TempData["AIsuccess"] = "New Area of Interest successfully created!";
                return RedirectToAction("Index");
            }
            else
            {
                return View(areaInterest);
            }
        }

        public ActionResult Delete(int? id)
        {
            if (HttpContext.Session.GetString("Role") != "admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            // Check conditions before deleting
            bool compExist = aiContext.IsCompetitionExist(id.Value);
            bool judgeExist = aiContext.IsJudgeExist(id.Value);
            if (compExist == true) //competition records found
            {
                // Display delete area of interest error message
                TempData["AIdeleteError"] = "There are competition records found, Area of Interest cannot be deleted.";
                return RedirectToAction("Index");
            }
            if (judgeExist == true) //judge records found
            {
                // Display delete area of interest error message
                TempData["AIdeleteError"] = "There are judge records found, Area of Interest cannot be deleted.";
                return RedirectToAction("Index");
            }
            else //no issues, can delete
            {
                aiContext.Delete(id.Value);
                // Display delete area of interest success message
                TempData["AIsuccess"] = "Area of Interest successfully deleted!";
                return RedirectToAction("Index");
            }
        }
    }
}
