﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;

namespace WEB_Assignment_Team2.Controllers
{
    public class ProfileController : Controller
    {
        private JudgeDAL judgeContext = new JudgeDAL();
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private List<SelectListItem> salutationlist = new List<SelectListItem>();
        private List<AreaInterest> listareainterest;
        //Method to populate dropdown list for judge.
        private void DataJudge(Judge judge)
        {
            // Adding salutation into a list
            List<string> salutation = new List<string> { null, "Dr", "Mr", "Ms", "Mrs", "Mdm" };
            foreach (string s in salutation)
            {
                if (Convert.ToString(judge.Salutation) == s)
                {
                    salutationlist.Add(new SelectListItem { Value = s, Text = s, Selected = true });
                }
                else
                {
                    salutationlist.Add(new SelectListItem { Value = s, Text = s, Selected = false });
                }
            }
            listareainterest = aiContext.GetAllAreaInterest();
            ViewData["areainterest"] = listareainterest.Find(a => a.AreaInterestID == judge.AreaInterestID).Name;
            ViewData["salutation"] = salutationlist;
        }
        private void DataCompetitor(Competitor competitor)
        {
            // Adding salutation into a list
            if (competitor != null)
            {
                // Adding salutation into a list
                List<string> salutation = new List<string> { null, "Dr", "Mr", "Ms", "Mrs", "Mdm" };
                foreach (string s in salutation)
                {
                    if (Convert.ToString(competitor.Salutation) == s)
                    {
                        salutationlist.Add(new SelectListItem { Value = s, Text = s, Selected = true });
                    }
                    else
                    {
                        salutationlist.Add(new SelectListItem { Value = s, Text = s, Selected = false });
                    }
                }
                ViewData["salutation"] = salutationlist;
            }
        }
        public ActionResult Competitor()
        {
            // display profile for competitor
            if ((HttpContext.Session.GetString("Role") != "competitor" ||
            HttpContext.Session.GetString("CompetitorID") == null))
            {
                return RedirectToAction("index", "home");
            }
            string id = HttpContext.Session.GetString("CompetitorID");
            int competitorID = Convert.ToInt32(id);
            Competitor competitor = competitorContext.GetCompetitor(competitorID);
            DataCompetitor(competitor);
            return View(competitor);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Competitor(Competitor competitor)
        {
            // changing the profile details for competitors
            if ((HttpContext.Session.GetString("Role") != "competitor" ||
            HttpContext.Session.GetString("CompetitorID") == null))
            {
                return RedirectToAction("index", "home");
            }
            if (ModelState.IsValid)
            {
                // data submited is valid
                competitorContext.UpdateProfile(competitor);
                DataCompetitor(competitor);
                return View(competitor);
            }
            else
            {
                // data submited is invalid
                competitor = competitorContext.GetCompetitor(competitor.CompetitorId);
                DataCompetitor(competitor);
                return View(competitor);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompetitorPassword(IFormCollection formData)
        {
            // changing password for competitors
            if ((HttpContext.Session.GetString("Role") != "competitor" ||
            HttpContext.Session.GetString("CompetitorID") == null))
            {
                return RedirectToAction("index", "home");
            }
            string oldPassword = formData["oldpassword"];
            string password = formData["Password"];
            string cfmpassword = formData["ConfirmPassword"];
            //Messages for user
            string opmessage = "";
            string cfmmessage = "";
            string message;
            Competitor target = competitorContext.AuthenticateCompetitor(HttpContext.Session.GetString("LoginEmail"), oldPassword);
            if (target == null)
            {
                // target is wrong
                opmessage = "Incorrect password.";
                message = "Failed to change password.";
            }
            else
            {
                if (password != cfmpassword) // checks if new password is confirmed
                {
                    message = "Failed to change password";
                    cfmmessage = "Confirm password does not match password.";
                }
                else
                {
                    message = "Successful password update.";
                    competitorContext.UpdatePassword(target.CompetitorId, password);
                }
            }
            TempData["oldPassword"] = opmessage;
            TempData["message"] = message;
            TempData["cfmessage"] = cfmmessage;
            return RedirectToAction("Competitor");
        }
        //Judge Portion
        //Judge Profile page
        public ActionResult Judge()
        {
            //If people with invalid credentials try to enter will be directed away from the page.
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "judge" ||
            HttpContext.Session.GetString("JudgeID") == null))
            {
                return RedirectToAction("index", "home");
            }
            //Using the judge id in string to get judge.
            string id = HttpContext.Session.GetString("JudgeID");
            int judgeid = Convert.ToInt32(id);
            //Password will be null in the judge object
            Judge judge = judgeContext.GetJudge(judgeid);
            DataJudge(judge);
            return View(judge);
        }
        //This function is to collect the model after user submits form.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Judge(Judge judge)
        {
            //If people with invalid credentials try to enter will be directed away from the page.
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "judge" ||
            HttpContext.Session.GetString("JudgeID") == null))
            {
                return RedirectToAction("index", "home");
            }
            if (ModelState.IsValid)
            {
                //Method to update Judge in database
                judgeContext.UpdateProfile(judge);
                //Return back the view
                DataJudge(judge);
                return View(judge);
            }
            else
            {
                //If ModelState is invalid then controller will get the 
                //unedited data from server again and send to view
                judge = judgeContext.GetJudge(judge.JudgeID);
                DataJudge(judge);
                return View(judge);
            }
        }
        //Password method is in the same page as Judge but in different form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Password(IFormCollection formData)
        {
            //If people with invalid credentials try to enter will be directed away from the page.
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "judge" ||
            HttpContext.Session.GetString("JudgeID") == null))
            {
                return RedirectToAction("index", "home");
            }
            string oldPassword = formData["oldpassword"];
            string password = formData["Password"];
            string cfmpassword = formData["ConfirmPassword"];
            //Messages for user
            string opmessage = "";
            string cfmmessage = "";
            string message;
            //Gets judge
            Judge target = judgeContext.AuthenticateJudge(HttpContext.Session.GetString("LoginEmail"), oldPassword);
            //Validation logic for updating password
            if (target == null)
            {
                opmessage = "Incorrect password.";
                message = "Failed to change password.";
            }
            else
            {
                if (password != cfmpassword)
                {
                    message = "Failed to change password";
                    cfmmessage = "Confirm password does not match password.";
                }
                else
                {
                    message = "Successful password update.";
                    judgeContext.UpdatePassword(target.JudgeID, password);
                }
            }
            TempData["oldPassword"] = opmessage;
            TempData["message"] = message;
            TempData["cfmessage"] = cfmmessage;
            return RedirectToAction("Judge");
        }
    }
}

