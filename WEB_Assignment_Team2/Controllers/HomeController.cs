﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WEB_Assignment_Team2.Models;
using WEB_Assignment_Team2.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace WEB_Assignment_Team2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private JudgeDAL judgeContext = new JudgeDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private List<SelectListItem> aoilist = new List<SelectListItem>();
        private List<AreaInterest> listareainterest;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://quotes.rest");
            HttpResponseMessage response = await client.GetAsync("/qod?category=inspire&language=en");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                dynamic result = JsonConvert.DeserializeObject<dynamic>(data);
                string quote = result.contents.quotes[0].quote;
                string author = result.contents.quotes[0].author;
                ViewData["quote"] = quote;
                ViewData["author"] = author;
                return View();
            }
            else
            {
                ViewData["quote"] = "";
                ViewData["author"] = "";
                return View();
            }
        }
        [Authorize]
        public async Task<ActionResult> LoginWithGoogle()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(
             OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                Regex rgx = new Regex(@"[A-Za-z0-9._%+-]+@lcu.edu.sg");
                if (rgx.IsMatch(eMail))
                {
                    return RedirectToAction("LogOut");
                }
                if (competitorContext.EmailValidate(eMail))
                {
                    // user's email already in database
                    HttpContext.Session.SetString("Role", "competitor");
                    HttpContext.Session.SetString("LoginEmail", eMail);
                    Competitor loginCompetitor = competitorContext.AuthenticateCompetitorWithGmail(eMail);
                    HttpContext.Session.SetString("CompetitorID", Convert.ToString(loginCompetitor.CompetitorId));
                }
                else
                {
                    // user's email is new to database
                    Competitor c = new Competitor()
                    {
                        CompetitorName = userName,
                        EmailAddr = eMail,
                        Salutation = "",
                        Password = "LOGINWITHGOOGLE"
                    };
                    int idNum = competitorContext.AddCompetitor(c);
                    HttpContext.Session.SetString("Role", "competitor");
                    HttpContext.Session.SetString("LoginEmail", eMail);
                    HttpContext.Session.SetString("CompetitorID", Convert.ToString(idNum));
                }
                return RedirectToAction("MyCompetition", "Competition");
            }
            catch (Exception e)
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }
        public IActionResult Login()
        {
            if (HttpContext.Session.GetString("Role") == "competitor")
            {
                //If Competitor tries to get to login page after they have logged in
                //Competitor will be directed to MyCompetition page
                return RedirectToAction("MyCompetition", "Competition");
            }
            else if (HttpContext.Session.GetString("Role") == "judge")
            {
                //If Judge tries to get to login page after they have logged in
                //Judge will be directed to MyCompetition page
                return RedirectToAction("MyCompetition", "Competition");
            }
            else if (HttpContext.Session.GetString("Role") == "admin")
            {
                //If Admin tries to get to login page after they have logged in
                //Admin will be directed to MyCompetition page
                return RedirectToAction("AllCompetitions", "Competition");
            }
            //If no session, will return login View
            return View();
        }

        [HttpPost]
        public IActionResult Login(IFormCollection formData)
        {
            //Get login Email and Password
            string loginEmail = formData["email"].ToString().ToLower();
            string password = formData["password"].ToString();
            //Get Judge if found in database.
            Judge loginJudge = judgeContext.AuthenticateJudge(loginEmail, password);
            Competitor loginCompetitor = competitorContext.AuthenticateCompetitor(loginEmail, password);
            //If else selection structure to check whether the person who signed in is a Judge/Competitor/Admin
            if (loginJudge != null) // if user a judge
            {
                HttpContext.Session.SetString("LoginEmail", loginEmail);
                HttpContext.Session.SetString("Role", "judge");
                HttpContext.Session.SetString("JudgeID", Convert.ToString(loginJudge.JudgeID));
                return RedirectToAction("MyCompetition", "Competition");
            }
            else if (loginCompetitor != null) // if user is a competitor
            {
                HttpContext.Session.SetString("LoginEmail", loginEmail);
                HttpContext.Session.SetString("Role", "competitor");
                HttpContext.Session.SetString("CompetitorID", Convert.ToString(loginCompetitor.CompetitorId));
                return RedirectToAction("MyCompetition", "Competition");
            }
            else if (loginEmail == "admin1@lcu.edu.sg" && password == "p@55Admin") // if user is a admin
            {
                HttpContext.Session.SetString("LoginEmail", loginEmail);
                HttpContext.Session.SetString("Role", "admin");
                return RedirectToAction("AllCompetitions", "Competition");
            }
            else // user login credetials not correct
            {
                TempData["loginerror"] = "Invalid login credentials";
                return View();
            }
            
        }
        public IActionResult Register()
        {
            //js used to hide if person selected competitor.
            //This is to set up the drop down list for area of interest.
            listareainterest = aiContext.GetAllAreaInterest();
            foreach (AreaInterest a in listareainterest)
            {
                aoilist.Add(new SelectListItem { Value = a.AreaInterestID.ToString(), Text = a.Name });
            }
            ViewData["AOI"] = aoilist;
            return View();
        }
        [HttpPost]
        public IActionResult Register(IFormCollection formData)
        {
            //Get data from formData
            string role = formData["role"];
            string name = formData["name"];
            string email = formData["email"].ToString().ToLower();
            string password = formData["password"];
            string salutation = formData["salutation"];
            //Return Value to the view should Register fails.
            ViewData["role"] = role;
            ViewData["name"] = name;
            ViewData["email"] = email;
            ViewData["password"] = password;
            ViewData["salutation"] = salutation;


            if (role == "competitor")
            {
                Regex rgx = new Regex(@"[A-Za-z0-9._%+-]+@lcu.edu.sg");
                if (rgx.IsMatch(email))
                {
                    TempData["emailfound"] = "Please enter a valid personal email address.";
                    return View();
                }
                Competitor c = new Competitor()
                {
                    CompetitorName = name,
                    EmailAddr = email,
                    Salutation = salutation,
                    Password = password
                };
                int idNum = competitorContext.AddCompetitor(c);

                if (idNum == -1) // email already exists
                {
                    TempData["emailfound"] = "Please enter a valid email address.";
                    return View();
                }
                else // new email
                {
                    HttpContext.Session.SetString("LoginEmail", email);
                    HttpContext.Session.SetString("Role", "competitor");
                    HttpContext.Session.SetString("CompetitorID", Convert.ToString(idNum));
                    return RedirectToAction("MyCompetition", "Competition");
                }
            }
            else if (role == "judge")
            {
                //Populate list again
                listareainterest = aiContext.GetAllAreaInterest();
                foreach (AreaInterest a in listareainterest)
                {
                    if (listareainterest.IndexOf(a) == Convert.ToInt32(formData["AOI"])-1)
                    {
                        aoilist.Add(new SelectListItem { Value = a.AreaInterestID.ToString(), Text = a.Name ,Selected=true});
                    }
                    else
                    {
                        aoilist.Add(new SelectListItem { Value = a.AreaInterestID.ToString(), Text = a.Name });
                    }
                    
                }
                ViewData["AOI"] = aoilist;
                //Check if email of judge has @lcu.edu.sg
                Regex rgx = new Regex(@"[A-Za-z0-9._%+-]+@lcu.edu.sg");
                if (!rgx.IsMatch(email))
                {
                    TempData["emailfound"] = "Please enter a valid judge email address.";
                    return View();
                }
                //Create New Judge
                int index = Convert.ToInt32(formData["AOI"]);
                Judge j = new Judge()
                {
                    JudgeName = name,
                    Salutation = salutation,
                    AreaInterestID = index,
                    EmailAddr = email,
                    Password = password,
                };
                //Add Judge to Database
                int idNum = judgeContext.AddJudge(j);
                if (idNum == -1)
                {
                    TempData["emailfound"] = "Please enter a valid email address.";
                    return View();
                }
                else
                {
                    HttpContext.Session.SetString("LoginEmail", email);
                    HttpContext.Session.SetString("Role", "judge");
                    HttpContext.Session.SetString("JudgeID", Convert.ToString(idNum));
                    return RedirectToAction("MyCompetition","Competition");
                }
            }
            return View();
        }
        //Clear session state when user click LogOut.
        public async Task<ActionResult> LogOut()
        {
            // Clear google authentication cookie
            await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);

            // Clear all key-value pairs stored in session state
            HttpContext.Session.Clear();
            // Go back to Home Page
            return RedirectToAction("Index");
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
