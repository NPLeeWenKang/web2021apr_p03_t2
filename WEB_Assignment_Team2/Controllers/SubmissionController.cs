﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_Assignment_Team2.DAL;
using WEB_Assignment_Team2.Models;
using System.IO;

namespace WEB_Assignment_Team2.Controllers
{
    public class SubmissionController : Controller
    {
        private AreaInterestDAL aiContext = new AreaInterestDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitionJudgeDAL compJudgeContext = new CompetitionJudgeDAL();
        private CompetitionSubmissionDAL compSubmissionContext = new CompetitionSubmissionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionScoreDAL competitionScoreContext = new CompetitionScoreDAL();
        private CommentDAL commentContext = new CommentDAL();
        private void SetAOI(Competition c)
        {
            List<AreaInterest> ailist = aiContext.GetAllAreaInterest();
            ViewData["AreaOfInterest"] = ailist.Find(a => a.AreaInterestID == c.AreaInterestID).Name;
        }
        private void SetCompetitorName(int cid)
        {
            string name = "";
            List<Competitor> cList = competitorContext.GetAllCompetitors();
            foreach(Competitor c in cList)
            {
                if (c.CompetitorId == cid)
                {
                    name = c.CompetitorName;
                }
                else { continue; }
            }
            ViewData["competitorName"] = name;
        }
        //Start Judge Portion
        public IActionResult List(int? id)
        {
            //Validation for right role, right judge for the right competition, competition existence
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (id == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition selectedCompetition = competitionContext.GetCompetition(id.Value);
                SetAOI(selectedCompetition);
                if (selectedCompetition == null ||
                    !compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), selectedCompetition.CompetitionID))
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                //prepare Model to be sent to view
                SubmissionViewModel csl = new SubmissionViewModel
                {
                    SelectedCompetition = selectedCompetition,
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(id.Value),
                    CompetitorList = competitorContext.GetAllCompetitors(),
                };
                List<CompetitionSubmission> csRemove = new List<CompetitionSubmission>();
                foreach (CompetitionSubmission cs in csl.CompetitionSubmissionList)
                {
                    if (cs.DateTimeFileUpload == null || cs.FileSubmitted == null)
                    {
                        csRemove.Add(cs);
                    }
                }
                foreach (CompetitionSubmission cs in csRemove)
                {
                    csl.CompetitionSubmissionList.Remove(cs);
                }
                return View(csl);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult CompetitorSubmission(int? competitionID, int? competitorID)
        {
            //Validation for right role, right judge for the right competition, competitionsubmission existence
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (competitionID == null || competitorID == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                if (!compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cs.CompetitionID)
                    || cs == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition c = competitionContext.GetCompetition(competitionID.Value);
                //Controls display of button to edit action.
                if(c.ResultReleasedDate>DateTime.Now && c.EndDate < DateTime.Now)
                {
                    TempData["Validity"] = "true";
                }
                else
                {
                    TempData["Validity"] = "false";
                }
                //prepare model to be sent to view
                SetCompetitorName(cs.CompetitorID);
                List<CompetitionScore> cscore = competitionScoreContext.GetScores(competitionID.Value, competitorID.Value);
                List<Criteria> cList = criteriaContext.GetCriteria(competitionID.Value);
                //To initialize the score
                if (cscore.Count < 1)
                {
                    foreach (Criteria criteria in cList)
                    {
                        CompetitionScore compscore = new CompetitionScore
                        {
                            CriteriaID = criteria.CriteriaID,
                            CompetitionID = cs.CompetitionID,
                            CompetitorID = cs.CompetitorID,
                            Score = 0,
                        };
                        competitionScoreContext.InitializeScore(compscore);
                        cscore.Add(compscore);
                    }
                }
                if (cList.Count == 0||cscore.Count ==0)
                {
                    return RedirectToAction("List", "Submission",new { id = competitionID.Value });
                }
                SubmissionScoreViewModel ssvm = new SubmissionScoreViewModel
                {
                    CompSub = cs,
                    CriteriaList = cList,
                    CsList = cscore,
                };
                return View(ssvm);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        public ActionResult Edit(int? competitionID, int? competitorID)
        {
            //Validation for right role, right judge for the right competition, competitionsubmission existence and the date and time 
            //valid to be edited
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                if (competitionID == null || competitorID == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                if (!compJudgeContext.AuthenticateCompJudge(Convert.ToInt32(HttpContext.Session.GetString("JudgeID")), cs.CompetitionID)
                    || cs == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                Competition c = competitionContext.GetCompetition(competitionID.Value);
                if (c.ResultReleasedDate < DateTime.Now || c.EndDate > DateTime.Now)
                {
                    return RedirectToAction("CompetitorSubmission", "Submission", new { competitionID = competitionID.Value, competitorID = competitorID.Value });
                }
                //Prepare model
                SetCompetitorName(cs.CompetitorID);
                List<CompetitionScore> cscore = competitionScoreContext.GetScores(competitionID.Value, competitorID.Value);
                List<Criteria> cList = criteriaContext.GetCriteria(competitionID.Value);
                //To initialize the score
                if (cscore.Count < 1)
                {
                    foreach (Criteria criteria in cList)
                    {
                        CompetitionScore compscore = new CompetitionScore
                        {
                            CriteriaID = criteria.CriteriaID,
                            CompetitionID = cs.CompetitionID,
                            CompetitorID = cs.CompetitorID,
                            Score = 0,
                        };
                        competitionScoreContext.InitializeScore(compscore);
                        cscore.Add(compscore);
                    }
                }
                if (cList.Count == 0 || cscore.Count == 0)
                {
                    return RedirectToAction("List", "Submission", new { id = competitionID.Value });
                }
                SubmissionScoreViewModel ssvm = new SubmissionScoreViewModel
                {
                    CompSub = cs,
                    CriteriaList = cList,
                    CsList = cscore,
                };
                return View(ssvm);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubmissionScoreViewModel ssvm)
        {
            //This action is to validate and update competitor's score.
            if (HttpContext.Session.GetString("Role") == "judge")
            {
                int competitionID = ssvm.CompSub.CompetitionID;
                Competition c = competitionContext.GetCompetition(competitionID);
                if (c.ResultReleasedDate < DateTime.Now || c.StartDate > DateTime.Now)
                {
                    return RedirectToAction("CompetitorSubmission", "Submission", new { competitionID = ssvm.CompSub.CompetitionID, competitorID = ssvm.CompSub.CompetitorID });
                }
                SetCompetitorName(ssvm.CompSub.CompetitorID);
                List<Criteria> cList = criteriaContext.GetCriteria(ssvm.CsList[0].CompetitionID);
                ssvm.CriteriaList = cList;
                if (!ModelState.IsValid)
                {
                    return View(ssvm);
                }
                foreach (CompetitionScore cscore in ssvm.CsList)
                {
                    competitionScoreContext.updateScore(cscore);
                }
                return RedirectToAction("CompetitorSubmission", "Submission", new { competitionID = ssvm.CompSub.CompetitionID, competitorID = ssvm.CompSub.CompetitorID });
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        //End of Judge Portion
        public ActionResult View(int? id)
        {
            // viewing of competitor's submission
            if (id == null)
            {
                return RedirectToAction("MyCompetition", "Competition");
            }
            
            if ((HttpContext.Session.GetString("Role") != null) &&
            (HttpContext.Session.GetString("Role") == "competitor"))
            {
                // checks whether to display join button
                if (compSubmissionContext.IsJoined(Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")), id.Value))
                {
                    ViewData["DisplayJoinButton"] = false;
                }
                else
                {
                    ViewData["DisplayJoinButton"] = true;
                }
                CompetitorSubmission cs = new CompetitorSubmission
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    SelectedCompetitionSubmission = compSubmissionContext.GetSubmission(id.Value, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID"))),
                    fileToUpload = null,
                };
                // Check whether competition exists
                if (cs.SelectedCompetition == null)
                {
                    return RedirectToAction("MyCompetition", "Competition");
                }
                // Set up Area of Interest in _CompetitionDetails
                SetAOI(cs.SelectedCompetition);
                return View(cs);
            }

            else
            {
                return RedirectToAction("index", "home");
            }
        }
        // public to view the details of submission , like names of competitors and the link to view their submission
        public ActionResult ViewDetails(int? id)
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                if (id == null)
                {
                    return RedirectToAction("AllCompetitions", "Competition");
                }

                PublicViewModel cs = new PublicViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(id.Value),
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(id.Value),
                    commentList = commentContext.GetCompComments(id.Value),
                    competitorList = competitorContext.GetAllCompetitors()

                };
                // Check whether competition exists
                if (cs.SelectedCompetition == null)
                {
                    return RedirectToAction("AllCompetitions", "Competition"); ;
                }
                // Set up Area of Interest in _CompetitionDetails
                SetAOI(cs.SelectedCompetition);
                return View(cs);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        //public to view competition submission
        // retrieve the relavant compid and competitor id on click
        public ActionResult PublicView(int? competitionID, int? competitorID)
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                if (competitionID == null || competitorID == null)
                {
                    return RedirectToAction("AllCompetitions", "Competition");
                }

                // bind the data to the relevant models
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                PublicViewModel ps = new PublicViewModel
                {
                    SelectedCompetition = competitionContext.GetCompetition(competitionID.Value),
                    CompetitionSubmissionList = compSubmissionContext.GetAllSubmissions(competitionID.Value),
                    CompSubmission = cs,
                    competitorList = competitorContext.GetAllCompetitors()
                };

                // Set up Area of Interest in _CompetitionDetails
                SetAOI(ps.SelectedCompetition);
                return View(ps);
            }

            else
            {
                return RedirectToAction("index", "home");
            }

        }

        // POST: Public to Create comment 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection formData)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            TempData["CommentSuccess"] = "Comment submitted!";
            if (HttpContext.Session.GetString("Role") == null)
            {
                int compid = Convert.ToInt32(formData["SelectedCompetition.CompetitionID"]);

                //Create new Comment , bind all the data to the model
                Comment c = new Comment
                {
                    CompetitionID = compid,
                    Description = formData["comment.Description"],
                    DateTimePosted = DateTime.Now,

                };
              
                //add comment to sql db
                commentContext.AddComment(c);
                // redirect to the view that holds the partial view and send the competitionId
                return RedirectToAction("ViewDetails", new { id = compid });
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        public ActionResult VoteForSubmission(int? competitionID, int? competitorID)
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                //getting the submission using the compid and competitorid
                CompetitionSubmission cs = compSubmissionContext.GetSubmission(competitionID.Value, competitorID.Value);
                // Check whether competition exists
                if (cs == null)
                {
                    return RedirectToAction("AllCompetitions", "Competition"); ;
                }
                return View(cs);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }
        //pub

        // POST: Public to vote for competitor 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VoteForSubmission(IFormCollection form)
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                // vote count to check. so initially the Session will store Voted as null.
                int? voteCount = HttpContext.Session.GetInt32("Voted");
                int competitionID = Convert.ToInt32(form["CompSubmission.CompetitionID"]);
                int competitorID = Convert.ToInt32(form["CompSubmission.CompetitorID"]);

                //as votecount is null, this block of codes will be executed to addlow user to vote
                if (voteCount == null)
                {
                    //Adding to VoteCount
                    compSubmissionContext.AddtoVote(form["CompSubmission.CompetitionID"], form["CompSubmission.CompetitorID"]);

                    // Session will then assign an integer to voted. from null to 1 , indicating the user has voted
                    HttpContext.Session.SetInt32("Voted", 1);
                    TempData["Success"] = "You have voted for this competitor!";

                    //After which redirect to the same page to show that the user has voted.
                    return RedirectToAction("PublicView", new { competitionID = competitionID , competitorID = competitorID});

                }
                // this will happen if the user has already voted in that session
                TempData["Failure"] = "You have voted already ! Unable to vote again.";
                return RedirectToAction("PublicView", new { competitionID = competitionID, competitorID = competitorID });
            }
            else
            {
                return RedirectToAction("index", "home");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadFile(CompetitorSubmission cs)
        {
            // allows user to submit their file
            if (cs.fileToUpload != null &&
            cs.fileToUpload.Length > 0)
            {
                try
                {
                    // Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(cs.fileToUpload.FileName);
                    // Rename the uploaded file with the staff’s name.
                    string uploadedFile = String.Format("File_{0}_{1}{2}", cs.SelectedCompetitionSubmission.CompetitionID, cs.SelectedCompetitionSubmission.CompetitorID,fileExt);
                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\submissions", uploadedFile);
                    // Upload the file to server
                    using (var fileSteam = new FileStream(
                     savePath, FileMode.Create))
                    {
                        await cs.fileToUpload.CopyToAsync(fileSteam);
                    }

                    // updates file name in database and datetime
                    cs.SelectedCompetitionSubmission.FileSubmitted = uploadedFile;
                    cs.SelectedCompetitionSubmission.DateTimeFileUpload = DateTime.Now;
                    compSubmissionContext.UpdateSubmissionFile(cs.SelectedCompetitionSubmission);
                    
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return RedirectToAction("View", new {id = cs.SelectedCompetitionSubmission.CompetitionID});
        }

        //public ActionResult VoteForSubmission()
        //{

        //}
    }
}
